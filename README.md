# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Tenere traccia di tutti i parametri necessari nel gioco di Munchkin ###

* Questo programma nasce con l'intenzione di fornire un valido supporto a un team di giocatori di Munchkin.
* Scritto in C
* Versione 0.1

### Funzioni implementate ###

* Aggiungi giocatore
* Visualizza l'elenco dei giocatori
* Modifica il livello o i potenziamenti di un giocatore
*  Distingui se un giocatore è un guerriero oppure no
* Affronta un combattimento: aggiungi o sottrai potenziamenti a una delle due parti

### Funzioni da aggiungere ###

* Visualizza la lista dei giocatori in ordine di forza
* Imposta il livello finale e notifica la vittoria di un giocatore
* Un giocatore corre in soccorso di un altro [OK 2014-09-29]
* Modifica nome giocatore [OK 2014-09-29]
* Scrittura su file
* Hall of fame, statistiche
* Tieni conto dei potenziamenti dei Dungeon
* Interfaccia grafica

### Come faccio ad usarlo? ###

* Nella sezione download o clonando il repo con git puoi ottenere un archivio con il codice sorgente e l'eseguibile già compilato. Tutto quello che devi fare è lanciare **maunchigioco**.
