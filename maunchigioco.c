#include <stdio.h>
#include <malloc.h>
#include <ctype.h>
#include <string.h>

/* Definisco i colori che verranno utilizzati */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m" /* menu principale */
#define KGRN  "\x1B[32m" /* giocatore */
#define KYEL  "\x1B[33m" /* lista giocatori */
#define KBLU  "\x1B[34m" /* credits */
#define KMAG  "\x1B[35m" /* aiuto giocatore */
#define KCYN  "\x1B[36m" /* mostro */
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

#define LEN 40 /* La lunghezza dei nomi */
#define YES c=='y' || c=='Y' /* In caso di domanda [Y/n] questi valori verranno presi come affermativi */

#define NOMEGIOCATORE KGRN,giocatore->nome,RESET

#define NOMEGIOCATORE1 KGRN,giocatore1->nome,RESET
#define NOMEAIUTANTE KMAG,giocatore2->nome,RESET
#define COLOREMOSTRO KCYN,RESET

typedef enum {false,true} boolean;

/* Definisco i tipi che terranno traccia delle caratteristiche del giocatore */
struct elemento {
  char nome[LEN];
  int livello;
  int potenziamenti;
  boolean guerriero;
  int combattimenti;
  int vittorie;
  struct elemento * next;
};

typedef struct elemento * ListaGiocatori;

boolean esci();
void stampaMenu();
void credits();
void visualizzaListaIntro(ListaGiocatori lista);
void visualizzaLista(ListaGiocatori lista);
void aggiungiGiocatore(ListaGiocatori * lista);
void modificaLivello(ListaGiocatori lista);
void modificaPotenziamenti(ListaGiocatori lista);
void modificaGuerriero(ListaGiocatori lista);
void combatti(ListaGiocatori lista);
void modificaNomeGiocatore(ListaGiocatori lista);

ListaGiocatori searchGiocatore(ListaGiocatori lista);

int modalita();
boolean cercaVincitore(ListaGiocatori giocatore, int livelloFinale);
void statisticheFinali(ListaGiocatori lista);

main() {
  char x; /*scelta nello switch */
  boolean fine=false;
  ListaGiocatori lista=NULL;

  printf("\nBenvenuto!\nQuesto programma ti aiuterà a gestire una partita di Munchkin.\n\n");
  int LivTermina = modalita();
  printf("Queste sono le opzioni possibili:\n\n");

  stampaMenu();
  while (!fine) {
    printf(KRED "\nMenu principale.%s Scegli un'opzione: ",RESET);
    scanf("%1s%*c", &x);
    switch(x) {
    case '1': case 'v': visualizzaListaIntro(lista); break;
    case '2': case 'a': aggiungiGiocatore(&lista); break;
    case '3': case 'l': modificaLivello(lista); break;
    case '4': case 'p': modificaPotenziamenti(lista); break;
    case '5': case 'w': modificaGuerriero(lista); break;
    case '6': case 'f': combatti(lista); break;
    case '7': modificaNomeGiocatore(lista); break;
    case 'q': fine=esci(); break;
    case 'h': printf("\nOpzioni possibili:\n\n"); stampaMenu(); break;
    case 'c': credits(); break;
    default: printf("Stolto, inserisci un'alternativa valida\n"); break;
    }
    int livelloFinale=LivTermina;
    fine = cercaVincitore(lista,livelloFinale); /* A ogni iterazione controlla se qualcuno ha vinto */
  }
  statisticheFinali(lista);
}

void stampaMenu(){
  printf("1,v: visualizza l'elenco dei giocatori\n");
  printf("2,a: aggiungi un giocatore\n");
  printf("3,l: modifica il livello di un giocatore\n");
  printf("4,p: modifica il valore dei potenziamenti di un giocatore\n");
  printf("5,w: specifica se un giocatore è un guerriero o no\n");
  printf("6,f: inizia un combattimento\n");
  printf("7: modifica il nome di un giocatore\n");
  printf("q: esci\n");
  printf("h: help\n");
  printf("c: credits\n");
}

boolean esci() {
  boolean fine;
  printf("Hai scelto di uscire. Sei sicuro? [Y/n] ");
  char c;
  scanf("%1s",&c);
  if (YES) {
    fine=true;
    printf("Addio!\n\n");
  }
  else {
    printf("Deciditi. Avresti potuto perdere tutti i dati della partita.\n");
    fine=false;
  }
  return fine;
}
void visualizzaListaIntro(ListaGiocatori lista) {
  printf(KYEL "\nLista dei giocatori\n" RESET);
  printf("\n**********\n");
  visualizzaLista(lista);
}

void statisticheFinali(ListaGiocatori lista) {
  printf(KYEL "\nStatisiche finali\n" RESET);
  printf("\n**********\n");
  visualizzaLista(lista);
}

void visualizzaLista(ListaGiocatori lista) {
    if (lista!=NULL) {
      printf("Giocatore: %s%s%s\n",KGRN,lista->nome,RESET);

      int livello = lista->livello;
      int potenziamenti = lista->potenziamenti;
      int forza = livello + potenziamenti;
      int combattimenti = lista->combattimenti;
      int favorevoli = lista->vittorie;
      char guerriero[3]; guerriero[2]='\0';
      if (lista->guerriero==true) {
	guerriero[0]='s';
	guerriero[1]='i';
      }
      else {
	guerriero[0]='n';
	guerriero[1]='o';
      }
      printf("Livello: %d\t\tPotenziamenti: %d\tTotale: %s%d%s\nHa sostenuto %d combattimenti, di cui %d favorevoli.\nGuerriero: %s\n\n",livello,potenziamenti,KCYN,forza,RESET,combattimenti,favorevoli,guerriero);

    visualizzaLista(lista->next);
  }
  else printf("\n**********\n");
}


void aggiungiGiocatore(ListaGiocatori * lista) {
  ListaGiocatori giocatore=(ListaGiocatori)malloc(sizeof(struct elemento));

  printf("Inserisci il nome del giocatore: ");
  scanf("%s",giocatore->nome);
    giocatore->livello=1;
    giocatore->potenziamenti=0;
    giocatore->guerriero=false;
    giocatore->combattimenti=0;
    giocatore->vittorie=0;
    giocatore->next=NULL;
  if (*lista==NULL)
    *lista=giocatore;
  else {   
    ListaGiocatori aux=*lista;
    while (aux->next!=NULL) aux=aux->next;
    aux->next=giocatore;
  }
printf("%s%s%s è stato aggiunto.\n\n",NOMEGIOCATORE);
}

void modificaLivello(ListaGiocatori lista) {
  ListaGiocatori giocatore = searchGiocatore(lista);

  if (giocatore!=NULL) {
    int incremento;
    printf("Di quanto vuoi aumentare/diminuire il livello? ");
    scanf("%d", &incremento);
    if (giocatore->livello + incremento <= 0) printf("Non puoi avere un livello negativo.\n");
    else giocatore->livello += incremento;
    int forza = giocatore->livello + giocatore->potenziamenti;
    printf("Fatto. Ora il livello di %s%s%s è %d. Totale della sua forza: %d\n",NOMEGIOCATORE,giocatore->livello,forza);
  }
}

void modificaPotenziamenti(ListaGiocatori lista) {
  ListaGiocatori giocatore = searchGiocatore(lista);

  if (giocatore!=NULL) {
    int incremento;
    printf("Di quanto vuoi aumentare/diminuire i potenziamenti? ");
    scanf("%d", &incremento);
    giocatore->potenziamenti += incremento;
    int forza = giocatore->livello + giocatore->potenziamenti;
    printf("Fatto. Ora i potenziamenti di %s%s%s valgono %d. Totale della sua forza: %d\n",NOMEGIOCATORE,giocatore->potenziamenti,forza);
  }
}

void modificaGuerriero(ListaGiocatori lista) {
  ListaGiocatori giocatore = searchGiocatore(lista);
  boolean stato;

  if (giocatore!=NULL) {
    if (lista->guerriero==true) {
      printf("%s%s%s è un guerriero. Vuoi togliergli la classe? [Y/n] ",NOMEGIOCATORE);
      char c;
      scanf("%1s",&c);
      if (YES) lista->guerriero=false;
    }
    else {
      printf("%s%s%s NON è un guerriero. Vuoi aggiungergli la classe? [Y/n] ",NOMEGIOCATORE);
      char c;
      scanf("%1s",&c);
      if (YES) lista->guerriero=true;
    }
    printf("Fatto.\n");
  }
}


void combatti(ListaGiocatori lista_giocatori) {
  ListaGiocatori giocatore1 = lista_giocatori;
  ListaGiocatori giocatore2 = NULL; /* Sarà usato per scorrere la lista alla ricerca del giocatore che aiuta */
  char c;
  int noncoincidono,liv_mostro,x,temp;
  int extra_giocatore=0, extra_mostro=0;
  char Nome[LEN],aiuto_giocatore[LEN];
  boolean aiuto=false,fine=false,trovato=false,fine_combattimento=false;

  printf("\nCombattimento!\nChi sta combattendo? ");
  giocatore1 = searchGiocatore(lista_giocatori);
  if (giocatore1 != NULL) {
    do { /* Inizializza il mostro */
      printf("Quale è il livello del %smostro%s? ",COLOREMOSTRO); 
      scanf("%d",&liv_mostro);
    }
    while (liv_mostro<=0);

    int totaleGiocatore;
    int totaleMostro;

    while (!fine) { /* Menu del combattimento */

      totaleGiocatore = giocatore1->livello + giocatore1->potenziamenti + extra_giocatore;
      if (aiuto) totaleGiocatore += (giocatore2->livello + giocatore2->potenziamenti);
      totaleMostro = liv_mostro + extra_mostro;

      printf("Situazione combattimento:\n\n");
      if (!aiuto) printf("%s%s%s: %d\n",NOMEGIOCATORE1,totaleGiocatore);
      else printf("%s%s%s e %s%s%s: %d\n",NOMEGIOCATORE1,NOMEAIUTANTE,totaleGiocatore);
      printf("%smostro%s: %d\n\n", COLOREMOSTRO,totaleMostro);
      printf("Cosa desideri fare? (0 fine combattimento, 1 modifica giocatore, 2 modifica mostro, 3 aiuto di un altro giocatore)\n");
      scanf("%d",&x); /* Chiedi cosa fare */

      switch(x) {

      case 0: { /* Fine combattimento */
	printf("Sei sicuro? [Y/n] ");
	scanf("%1s",&c);
	if (YES) {
	  fine=true;
	  printf("Il combattimento è finito!\n");

	  if (totaleGiocatore > totaleMostro || giocatore1->guerriero==true && totaleGiocatore >= totaleMostro) { /* Se il giocatore o i giocatori vincono */
	    if (!aiuto) printf("%s%s%s ha vinto! Il %smostro%s è stato sconfitto!\n",NOMEGIOCATORE1,COLOREMOSTRO);
	    else printf("%s%s%s e %s%s%s hanno vinto! Il %smostro%s è stato sconfitto!\n",NOMEGIOCATORE1,NOMEAIUTANTE,COLOREMOSTRO);

	    printf("Quanti livelli guadagna %s%s%s? ",NOMEGIOCATORE1); /* In caso di vittoria aumento il livello dei combattenti */
	      do 
		scanf("%d",&temp);
	      while (temp<=0);
	      giocatore1->livello+=temp;
	      printf("Ora %s%s%s è al livello %d\n",NOMEGIOCATORE1,giocatore1->livello);

	      if (aiuto) {
		printf("Quanti livelli guadagna %s%s%s? ",NOMEAIUTANTE);
		do 
		  scanf("%d",&temp);
		while (temp<=0);
		giocatore2->livello+=temp;
		printf("Ora %s%s%s è al livello %d\n",NOMEAIUTANTE,giocatore2->livello);
	      }
	  }
	  else /* se perdono */
	    if (!aiuto) printf("%s%s%s è stato sconfitto. Ora gli aspettano delle brutte cose. MUAHAHAHAHAHAH\n",NOMEGIOCATORE1);
	    else printf("%s%s%s e %s%s%s sono stati sconfitti. Ora subiranno delle brutte cose. MUAHAHAHAHAHAH\n",NOMEGIOCATORE1,NOMEAIUTANTE);
	}
	else printf("Deciditi\n");
	break;
      }

      case 1: { /* Modifica la forza del giocatore */
	if (!aiuto) printf("Aumenta/diminuisci la forza di %s%s%s di ",NOMEGIOCATORE1);
	else printf("Aumenta/diminuisci la forza di %s%s%s e di %s%s%s di ",NOMEGIOCATORE1,NOMEAIUTANTE);
	scanf("%d",&temp);
	extra_giocatore+=temp;
	break;
      }

      case 2: { /* Modifica la forza del mostro */
	printf("Aumenta/diminuisci la forza del %smostro%s di ",COLOREMOSTRO);
	scanf("%d",&temp);
	extra_mostro+=temp;
	break;
      }

      case 3: { /* Un altro giocatore corre in aiuto */
	giocatore2=lista_giocatori;
	printf("Quale giocatore corre in aiuto di %s%s%s? ",NOMEGIOCATORE1);
	scanf("%s",&aiuto_giocatore);

        trovato=false;
	while (giocatore2!=NULL && !trovato) { /* Scandisci la lista finché non trovi il giocatore aiutante */
	  noncoincidono=strcmp(aiuto_giocatore,giocatore2->nome); /* strcmp restituisce 0 se le due stringhe sono uguali, un altro intero se sono diverse */
	  if (noncoincidono)
	    giocatore2=giocatore2->next;
	  else trovato=true;
	}
	if (giocatore2==NULL) printf("Hey, dammi un nome valido.\n");
	else {
	  aiuto=true;
          totaleGiocatore += (giocatore2->livello + giocatore2->potenziamenti);
	  printf("%s%s%s si è schierato a fianco di %s%s%s! Ora la forza complessiva dei giocatori è %d.\n",NOMEAIUTANTE,NOMEGIOCATORE1,totaleGiocatore);
	}
	break;
      }

      default: printf("Valore non valido.\n");
      }
    }
  }
}

ListaGiocatori searchGiocatore(ListaGiocatori lista) {
  boolean trovato=false;
  int noncoincidono;
  char Nome[LEN];
  printf("Nome del giocatore: ");
  scanf("%s",Nome);
  
  while (lista!=NULL && !trovato) {
    noncoincidono=strcmp(Nome,lista->nome);
    if (noncoincidono)
      lista=lista->next;
    else
      trovato=true;
  }
  if (!trovato) printf("Inetto, dammi un nome valido.\n");
  
  return lista;
}

void modificaNomeGiocatore(ListaGiocatori lista) {
  char nuovoNome[LEN];
  ListaGiocatori giocatore = searchGiocatore(lista); 
  if (giocatore!=NULL) {
    printf("Hai scelto di modificare %s%s%s. Inserisci il nuovo nome: ",NOMEGIOCATORE);
    scanf("%s",nuovoNome);
    strcpy(giocatore->nome,nuovoNome);
    printf("Fatto. Ora il giocatore si chiama %s%s%s.\n",NOMEGIOCATORE);
  }
}

int modalita() {
  int livello;
  int i;
  int coincide;
  char mod[LEN];
  char epica[]={'e','p','i','c','a'}, normale[]={'n','o','r','m','a','l','e'};
  printf("In quale modalità vuoi giocare? Epica o normale? ");
  scanf("%s",mod);
  for (i=0;i<LEN;i++) mod[i]=tolower(mod[i]);
  coincide = strncmp(mod,epica,5);
  if (coincide==0) {
    printf("Hai scelto modalità epica. Certo che non hai proprio niente di meglio da fare nelle prossime sei ore eh?\n\n");
    livello=20;
  }
  else {
    coincide = strncmp(mod,normale,7);
    if (coincide==0) {
      printf("Hai scelto modalità normale. Vince chi arriva prima al livello 10.\n\n");
      livello=10;
    }
    else {
      printf("Immissione non valida. È stato fissato a 10 il livello della vittoria. Puoi comunque modificarlo a mano se lo desideri.\n\n");
      livello=10;
    }
  }
  return livello;
}

boolean cercaVincitore(ListaGiocatori giocatore, int livelloFinale) {
  boolean finePartita;
  if (giocatore!=NULL) {
    if (giocatore->livello >= livelloFinale) {
      printf("\n**********\n\n");
      printf("%s%s%s È IL VINCITORE!!\n",NOMEGIOCATORE);
      printf("\n**********\n\n");
      finePartita=true;
    }
    else {
      giocatore=giocatore->next;
      finePartita = cercaVincitore(giocatore, livelloFinale);
    }
  }
  else finePartita = false;
  return finePartita;
}

void credits() {
  printf("\nCreated by %sGiulio Pasqualetti%s <me@giuliopasqualetti.it> in Sep 2014.\n",KBLU,RESET);
  printf("\n");
  printf("This program is free software: you can redistribute it and/or modify\n");
  printf("it under the terms of the GNU General Public License as published by\n");
  printf("the Free Software Foundation, either version 3 of the License, or\n");
  printf("(at your option) any later version.\n");
  printf("\n");
  printf("This program is distributed in the hope that it will be useful,\n");
  printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
  printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
  printf("GNU General Public License for more details.\n");
  printf("\n");
  printf("You should have received a copy of the GNU General Public License\n");
  printf("along with this program.  If not, see <http://www.gnu.org/licenses/>.\n");
  printf("\n");
}
